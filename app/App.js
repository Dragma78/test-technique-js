import React from 'react';
import {Button, Picker, StyleSheet, TextInput, View} from 'react-native';
import Data from './Data.js';

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            ip: '',
            port: '',
            turns: '',
            sentence: '',
            lang: ''
        };
    }

    render() {
        return (
            <View style={styles.container}>
                <TextInput
                    value={this.state.ip}
                    onChangeText={(ip) => this.setState({ip})}
                    placeholder={'IP'}
                    style={styles.input}
                />
                <TextInput
                    value={this.state.port}
                    onChangeText={(port) => this.setState({port})}
                    placeholder={'PORT'}
                    style={styles.input}
                />
                <TextInput
                    value={this.state.username}
                    onChangeText={async (username) => await this.setState({username})}
                    placeholder={'Username'}
                    style={styles.input}
                />
                <TextInput
                    value={this.state.sentence}
                    onChangeText={(sentence) => this.setState({sentence})}
                    placeholder={'Write sentence or word here'}
                    style={styles.input}
                />
                <Picker
                    selectedValue={this.state.lang}
                    style={styles.dropdown}
                    onValueChange={(itemValue) =>
                        this.setState({lang: itemValue})
                    }>
                    {Object.keys(Data.options).map((key) => {
                        return (<Picker.Item label={Data.options[key]} value={key} key={key}/>)
                    })}
                </Picker>
                <Picker
                    selectedValue={this.state.turns}
                    style={styles.dropdown}
                    onValueChange={(itemValue) =>
                        this.setState({turns: itemValue})
                    }>
                    {Object.keys(Data.turns).map((key) => {
                        return (<Picker.Item label={Data.turns[key]} value={key} key={key}/>)
                    })}
                </Picker>
                <Button
                    onPress={async () => {
                        if (!this.state.username || !this.state.ip || !this.state.port || !this.state.turns || !this.state.lang || !this.state.sentence) {
                            alert("PLEASE FILL ALL FIELDS !");
                            console.log("Button click");
                            return;
                        }
                        let url = "http://" + this.state.ip +
                            ":" + this.state.port +
                            "/process?turns=" + this.state.turns +
                            "&name=" + this.state.username +
                            "&base=" + this.state.sentence +
                            "&lang=" + this.state.lang;
                        await fetch(url, {
                            method: 'GET',
                            headers: {"Content-Type": "application/json"}
                        }).then(async (response) => {
                            const statusCode = response.status;
                            const data = await response.json();
                            return await Promise.all([statusCode, data]);
                        }).then(([res, data]) => {
                            console.log(res, data);
                            alert(data.player +
                                "\nNumber of turns: " + data.turns +
                                "\nYour text: " + data.baseSentence +
                                "\nResult text: " + data.finalSentence +
                                "\nNote: " + data.note);
                        })
                    }}
                    title="Click Me"
                    color="#841584"
                />
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ffffff',
    },
    input: {
        width: 250,
        height: 44,
        padding: 10,
        marginBottom: 10,
        backgroundColor: '#ecf0f1'
    },
    dropdown: {
        height: 50,
        width: 250,
        alignItems: 'center',
        justifyContent: 'center'
    }
});