const mongoose = require('mongoose');
const rules = require('./schema');

let db = mongoose.connect('mongodb://localhost/technicaltest', {useNewUrlParser: true});
const game = mongoose.model('game', rules, 'games');

module.exports = game;