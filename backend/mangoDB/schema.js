const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const gameSchema = new Schema({
    turns: Number,
    player: String,
    baseSentence: String,
    finalSentence: String,
    baseLanguage: String,
    intermediateLanguages: [String],
    intermediateSentences: [String],
    intermediateAudioFile: [String],
    note: Number,
    date: {type: Date, default: Date.now},
});

module.exports = gameSchema;