const express = require('express');
const game = require('../mangoDB/mango_connection');
const router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  game.find().sort({ note: 'desc' }).lean().exec(function (err, list) {
    return res.json(list);
  });
});

module.exports = router;