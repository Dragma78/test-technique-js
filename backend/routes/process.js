const express = require('express');
const router = express.Router();
const game = require('../mangoDB/mango_connection');
const t2s = require('../ibmModules/textToSpeech');
const s2t = require('../ibmModules/speechToText');
const fs = require('fs');
const dl = require("damerau-levenshtein-js");

async function setRules(req, data) {
    data.turns = req.query.turns;
    data.playerName = req.query.name;
    data.base = req.query.base.replace('+', ' ');
    data.lang = req.query.lang;
    data.interSent = [];
    data.interLang = [];
    data.interAudio = [];
    data.actSent = '';
    data.fileName = '';
    data.actLang = '';
    data.interSent.push(data.base);
    data.note = 0;
}


function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        let r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

async function createFileName(data) {
    data.fileName = './public/audios/' + uuidv4() + '.wav';
}

async function getLanguage(data) {
    data.actLang = t2s.getRandomLang();
    while (data.lang.search(data.actLang.substring(0, 4)) !== -1) {
        data.actLang = t2s.getRandomLang();
    }
}

async function main(data) {
    console.log("file: " + data.fileName + " created");
    await t2s.synthText(data.interSent, data.actLang, data.fileName, t2s.textToSpeech);
    console.log("IBM'S API text-to-speech call: OK !");
    try {
        await s2t.recognizeSpeech(data.fileName, data.lang, data.base, s2t.speechToText, data);
    } catch (e) {
        console.log("IBM'S API speech-to-text call: KO !");
        throw TypeError
    }
    console.log("IBM'S API speech-to-text call: OK !");
}

async function saveIntermediatesResults(data) {
    data.interLang.push(data.actLang);
    data.interSent.push(data.actSent);
    data.interAudio.push(data.fileName);
}

async function runGame(data) {
    for (let i = 0; i < data.turns; i++) {
        await getLanguage(data);
        await createFileName(data);
        try {
            await main(data);
        } catch (e) {
            i = i - 1;
            fs.unlink(data.fileName, (err => {
                if (err) console.log("File deleted ! " + data.fileName);
            }));
            data.actSent = '';
            data.fileName = '';
            data.actLang = '';
            continue
        }
        await saveIntermediatesResults(data);
    }
    await data.interSent.shift();
}

async function createResults(data) {
    let final = data.interSent[data.interSent.length - 1];
    data.note = dl.distance(data.base, final);
    data.interSent.pop();
    data.results = {
        turns: data.turns,
        player: data.playerName,
        baseSentence: data.base,
        finalSentence: final,
        baseLanguage: data.lang,
        intermediateLanguages: data.interLang,
        intermediateSentences: data.interSent,
        intermediateAudioFile: data.interAudio,
        note: data.note
    };
}

async function saveResults(data) {
    let test = new game(data.results);
    await test.save(function (err) {
        if (err) return console.log('Error while processing the save');
        console.log('New game saved !')
    });
}

router.get('/process', async function (req, res, next) {
    let data = {
        turns: null,
        playerName: null,
        base: null,
        lang: null,
        interLang: null,
        interSent: null,
        interAudio: null,
        actSent: null,
        fileName: null,
        actLang: null,
        results: null,
        note: null
    };

    await setRules(req, data);
    await runGame(data);
    await createResults(data);
    await saveResults(data);
    await res.json(data.results);
});

module.exports = router;