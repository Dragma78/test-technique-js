const SpeechToTextV1 = require('ibm-watson/speech-to-text/v1');
const fs = require('fs');

const speechToText = new SpeechToTextV1({
    iam_apikey: 'xzXE87VPiKaWClvz8sz38Cg63i8Acsb63OKfIw8q05OV',
    url: 'https://gateway-lon.watsonplatform.net/speech-to-text/api',
    disable_ssl_verification: true,
});

arrayLang = [
    'ar-AR_BroadbandModel',
    'de-DE_BroadbandModel',
    'de-DE_NarrowbandModel',
    'en-GB_BroadbandModel',
    'en-GB_NarrowbandModel',
    'en-US_BroadbandModel',
    'en-US_NarrowbandModel',
    'en-US_ShortForm_NarrowbandModel',
    'es-AR_BroadbandModel',
    'es-AR_NarrowbandModel',
    'es-CL_BroadbandModel',
    'es-CL_NarrowbandModel',
    'es-CO_BroadbandModel',
    'es-CO_NarrowbandModel',
    'es-ES_BroadbandModel',
    'es-ES_NarrowbandModel',
    'es-MX_BroadbandModel',
    'es-MX_NarrowbandModel',
    'es-PE_BroadbandModel',
    'es-PE_NarrowbandModel',
    'fr-FR_BroadbandModel',
    'fr-FR_NarrowbandModel',
    'ja-JP_BroadbandModel',
    'ja-JP_NarrowbandModel',
    'ko-KR_BroadbandModel',
    'ko-KR_NarrowbandModel',
    'pt-BR_BroadbandModel',
    'pt-BR_NarrowbandModel',
    'zh-CN_BroadbandModel',
    'zh-CN_NarrowbandModel'
];

async function setRecognizeParams(name, lang, base) {
    return {
        audio: await fs.createReadStream(name),
        content_type: 'audio/wav',
        model: arrayLang[await arrayLang.indexOf(lang)],
        word_alternatives_threshold: 0.9,
        keywords: [base],
        keywords_threshold: 0.5,
    }
}

async function recognizeSpeech(fileName, lang, base, ibmFunc, data) {
    let recognizeParams = await setRecognizeParams(fileName, lang, base);
    await ibmFunc.recognize(recognizeParams)
        .then(async speechRecognitionResults => {
            data.actSent = await JSON.stringify(speechRecognitionResults.results[0].alternatives[0].transcript, null, 2).substr(1).slice(0, -1);
            await console.log(data.actSent);
        })
        .catch(err => {
            console.log('error:', err);
            throw TypeError;
        });
}

module.exports = {
    speechToText,
    recognizeSpeech
};