# Test Technique en JS

Le but de ce test est de créer une mini application en nodes-JS et react-native pour "jouer" au "Bouche à oreille" en utilisant l'API text-to-speech et speech-to-text de IBM en ajoutant un système de traduction entre les tours pour une difficulté supplémentaire.

> ### Déroulement d'une partie
>
>-   Au début de la partie un nombre de tour est décidé par l'utilisateur.
>-   L'utilisateur écrit ensuite une phrase / paragraphe de la taille de son choix, et determine la langue du texte.
>-   Le tour est composé :
>    -   D'un  [text to speech](https://www.ibm.com/watson/services/text-to-speech/)  dans une langue différente de la langue du texte.
>    -   Puis d'un  [speech to text](https://www.ibm.com/watson/services/speech-to-text/)  dans la langue du texte
>-   À chaque tour, le texte généré ainsi qu'une note comparative (sur l'échelle de votre choix, avec l'algorithme de votre choix) par rapport au texte précédent doit être sauvegardé sur un serveur distant (db, fichier plat, jpg, comme vous voulez :))
>-   En fin de partie, un récapitulatif des tours devra être affiché, ainsi que la note globale.
>
>[https://github.com/dragma/technical-test-2k19](https://github.com/dragma/technical-test-2k19)
> -Dragma-

*ATTENTION: cette application nodeJS et react-native est écrite sous windows, les scripts dans les package.json sont donc fait pour windows* 

# Lancement de l'application

## Prérequis

- Avoir installer l'application "Expo" sur son smartphone
- Avoir installer Node.js et MongoDB sur son ordinateur

## Execution

- Ouvrir un terminal et se placer dans le dossier "backend" à la racine du repository
    - Si c'est la premiere fois que vous accedez à ce dossier, executer " ?> npm install "
    - Executer " ?> npm start ", le serveur Node.JS est maintenant en cours d'execution
    
- Ouvrir un second terminal et se placer dans le dossier "app" à la racine du repository
    - Si c'est la premiere fois que vous accedez à ce dossier, executer " ?> npm install "
    - Executer " ?> expo start ", une fenêtre contenant la console de l'application s'ouvre dans votre navigateur internet favoris
    - Ouvrir l'application expo et scanner le code QR pour acceder à l'application sur votre smartphone
    
![ServerRunning](./images/serveur1.png)
![AppRunning](./images/app1.png)

Une fois dans l'application, entrer l'IP de l'ordinateur hébergeant le serveur dans la case IP et si vous n'avez rien changé 3000 dans la case PORT
Ensuite renseigner un nom d'utilisateur et un texte a envoyer.
![ServerRunning](./images/app2.png)
Enfin selectionner dans les menus déroulants la langue de votre texte et le nombre de tours à effectuer.
![ServerRunning](./images/app3.png)
Si rien n'est renseigner ou qu'un champ est mal remplie une erreur s'affichera
Sinon une alerte s'ouvrira et vous donnera les résultats.
![ServerRunning](./images/app4.png)

Bon jeu !





